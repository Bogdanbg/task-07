package com.epam.rd.java.basic.task7;

public abstract class DBConstants {

    private DBConstants() {

    }

    public static final String GET_ALL_USERS = "SELECT id, login FROM users";
    public static final String GET_ALL_TEAMS = "SELECT id, name FROM teams";
    public static final String INSERT_INTO_USERS = "INSERT INTO users (login) VALUES ('";
    public static final String DELETE_FROM_USERS = "DELETE FROM users WHERE id IN (";
    public static final String GET_USER = "SELECT id, login FROM users WHERE login = '";
    public static final String GET_TEAM = "SELECT id, name FROM teams WHERE name = '";
    public static final String INSERT_INTO_TEAMS = "INSERT INTO teams (name) VALUES ('";
    public static final String INSERT_INTO_USER_TEAMS = "INSERT INTO users_teams (user_id, team_id) VALUES(";
    public static final String GET_USER_TEAMS = "SELECT team_id, name FROM users_teams JOIN teams ON team_id = id WHERE user_id = ";
    public static final String DELETE_FROM_TEAMS = "DELETE FROM teams WHERE name = '";
    public static final String UPDATE_TEAMS = "UPDATE teams SET name = '";


}
