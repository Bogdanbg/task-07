package com.epam.rd.java.basic.task7.db.entity;


import java.util.Objects;

public class User {

	private int id;

	private String login;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public User() {

	}

	public User(int id, String login) {
		this.id = id;
		this.login = login;
	}

	public static User createUser(String login) {
		User user = new User();
		user.setLogin(login);
		return user;
	}

	@Override
	public String toString() {
		return "User{" + "login='" + login + '\''+ '}' ;
	}

	@Override
	public boolean equals(Object obj) {
		User user = (User) obj;
		return Objects.equals(login, user.login);
	}
}
