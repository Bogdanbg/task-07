package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.DBConstants;
import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.StringJoiner;

public class DBManager {

	private static DBManager instance;

	private static String FULL_URL = "jdbc:mysql://localhost:3306/test2db?user=root&password=Bogdan9$";


	public static synchronized DBManager getInstance() {
		if (instance==null)
				instance = new DBManager();
		return instance;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		FileInputStream fis;
		Properties property = new Properties();
		try
		{
			fis = new FileInputStream("app.properties");
			property.load(fis);
			FULL_URL = property.getProperty("connection.url");
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try (Connection connection = DriverManager.getConnection(FULL_URL);
			 Statement statement = connection.createStatement();
			 ResultSet resultSet = statement.executeQuery(DBConstants.GET_ALL_USERS))
		{
			while (resultSet.next()){
				User user = new User();
				user.setId(resultSet.getInt("id"));
				user.setLogin(resultSet.getString("login"));
				users.add(user);

			}
		} catch (SQLException e) {

			e.printStackTrace();
			throw new DBException("db exception",e);
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		if (user == null) return false;
		FileInputStream fis;
		Properties property = new Properties();
		try
		{
			fis = new FileInputStream("app.properties");
			property.load(fis);
			FULL_URL = property.getProperty("connection.url");
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try (Connection connection = DriverManager.getConnection(FULL_URL);
			 Statement statement = connection.createStatement()){
			if (1 == statement.executeUpdate(DBConstants.INSERT_INTO_USERS + user.getLogin() + "')", Statement.RETURN_GENERATED_KEYS));
				try (ResultSet resultSet = statement.getGeneratedKeys()){
					resultSet.next();
					user.setId(resultSet.getInt(1));
					return true;
				}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("failed to insert",e);
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
		if (users.length == 0 ) return false;

		StringJoiner stringJoiner = new StringJoiner(",",DBConstants.DELETE_FROM_USERS,")");
		for (User user :
				users) {
			if (user != null) stringJoiner.add(String.valueOf(user.getId()));
		}
		FileInputStream fis;
		Properties property = new Properties();
		try
		{
			fis = new FileInputStream("app.properties");
			property.load(fis);
			FULL_URL = property.getProperty("connection.url");
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try (Connection connection = DriverManager.getConnection(FULL_URL);
			 Statement statement = connection.createStatement()){
			return statement.executeUpdate(stringJoiner.toString()) > 0;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("delete failure",e);
		}
	}

	public User getUser(String login) throws DBException {
		if (login == null) return null;

		FileInputStream fis;
		Properties property = new Properties();
		try
		{
			fis = new FileInputStream("app.properties");
			property.load(fis);
			FULL_URL = property.getProperty("connection.url");
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try (Connection connection = DriverManager.getConnection(FULL_URL);
			 ResultSet resultSet = connection.createStatement().executeQuery(DBConstants.GET_USER + login + "'")){
			if (resultSet.next()){
				User user = User.createUser(resultSet.getString("login"));
				user.setId(resultSet.getInt("id"));
				return user;
			}
			return null;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("get user failure",e);
		}
	}

	public Team getTeam(String name) throws DBException {
		if (name == null) return null;

		FileInputStream fis;
		Properties property = new Properties();
		try
		{
			fis = new FileInputStream("app.properties");
			property.load(fis);
			FULL_URL = property.getProperty("connection.url");
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try (Connection connection = DriverManager.getConnection(FULL_URL);
			 ResultSet resultSet = connection.createStatement().executeQuery(DBConstants.GET_TEAM + name + "'")){
			if (resultSet.next()){
				Team team = Team.createTeam(resultSet.getString("name"));
				team.setId(resultSet.getInt("id"));
				return team;
			}
			return null;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("get team failure",e);
		}
	}

	public List<Team> findAllTeams() throws DBException {

		List<Team> teams = new ArrayList<>();
		FileInputStream fis;
		Properties property = new Properties();
		try
		{
			fis = new FileInputStream("app.properties");
			property.load(fis);
			FULL_URL = property.getProperty("connection.url");
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			Connection connection = DriverManager.getConnection(FULL_URL);
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(DBConstants.GET_ALL_TEAMS);
			while (resultSet.next()){
				Team team = Team.createTeam(resultSet.getString("name"));
				team.setId(resultSet.getInt("id"));
				teams.add(team);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("find all teams failure",e);
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		if (team == null) return false;

		FileInputStream fis;
		Properties property = new Properties();
		try
		{
			fis = new FileInputStream("app.properties");
			property.load(fis);
			FULL_URL = property.getProperty("connection.url");
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try (Connection connection = DriverManager.getConnection(FULL_URL);
			 Statement statement = connection.createStatement()){
			if (1 == statement.executeUpdate(DBConstants.INSERT_INTO_TEAMS + team.getName()+ "')", Statement.RETURN_GENERATED_KEYS));
			try (ResultSet resultSet = statement.getGeneratedKeys()){
				resultSet.next();
				team.setId(resultSet.getInt(1));
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("insert team failure", e);
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		if (user == null || teams.length == 0) return false;

		FileInputStream fis;
		Properties property = new Properties();
		try
		{
			fis = new FileInputStream("app.properties");
			property.load(fis);
			FULL_URL = property.getProperty("connection.url");
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Connection connection = null;

		PreparedStatement statement = null;

		try {
			connection = DriverManager.getConnection(FULL_URL);
			connection.setAutoCommit(false);
			statement = connection.prepareStatement(DBConstants.INSERT_INTO_USER_TEAMS+ user.getId()+ ",?)");
			boolean isInserted = false;
			for (Team team :
					teams) {
				if (team != null){
					statement.setInt(1,team.getId());
					isInserted = (statement.executeUpdate()>0) || isInserted;
				}
			}

			connection.commit();
			return isInserted;
		} catch (SQLException e) {
			e.printStackTrace();
			tryRollback(connection);
			throw new DBException("set teams failure",e);

		}finally {
			tryClose(statement);
			tryClose(connection);
		}
	}

	private void tryRollback(Connection connection){
		if (connection != null){
			try {
				connection.rollback();
			}catch (SQLException exception){
				exception.printStackTrace();
			}
		}
	}
	private void tryClose(AutoCloseable connect){
		if (connect != null) {
			try {
				connect.close();
			}catch (Exception e){
				e.printStackTrace();
			}
		}
	}


	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();

		FileInputStream fis;
		Properties property = new Properties();

		try
		{
			fis = new FileInputStream("app.properties");
			property.load(fis);
			FULL_URL = property.getProperty("connection.url");
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}


		try (Connection connection = DriverManager.getConnection(FULL_URL);
			 ResultSet resultSet = connection.createStatement().executeQuery(DBConstants.GET_USER_TEAMS+ user.getId())){
			while (resultSet.next()){
				Team team = Team.createTeam(resultSet.getString("name"));
				team.setId(resultSet.getInt("team_id"));
				teams.add(team);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("get user team failure", e);
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		if (team == null) return false;

		FileInputStream fis;
		Properties property = new Properties();

		try
		{
			fis = new FileInputStream("app.properties");
			property.load(fis);
			FULL_URL = property.getProperty("connection.url");
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try (Connection connection = DriverManager.getConnection(FULL_URL);
			 Statement statement = connection.createStatement()){
			return statement.executeUpdate(DBConstants.DELETE_FROM_TEAMS + team.getName() + "'") > 0;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("delete team failure", e);
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		if (team == null) return false;

		FileInputStream fis;
		Properties property = new Properties();


		try
		{
			fis = new FileInputStream("app.properties");
			property.load(fis);
			FULL_URL = property.getProperty("connection.url");
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try (Connection connection = DriverManager.getConnection(FULL_URL);
			 Statement statement = connection.createStatement()){
			return statement.executeUpdate(DBConstants.UPDATE_TEAMS + team.getName() + "'WHERE id = "+ team.getId()) > 0;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("update team failure", e);
		}
	}

}
